import Quick
import CodingChallengeByMacNoble

extension QuickSpec {
  
  func jsonDictionaryFromFile(_ name: String) -> JSONDictionary {
    return CodingChallengeByMacNoble.jsonDictionaryFromFile(name, bundle: Bundle(for: type(of: self)))
  }
  
}
