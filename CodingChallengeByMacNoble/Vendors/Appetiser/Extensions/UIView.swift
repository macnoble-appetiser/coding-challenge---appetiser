import UIKit

extension UIView {
  
  func addDropShadow(
    color: UIColor,
    opacity: Float = 0.5,
    offSet: CGSize = CGSize(width: -1, height: 1),
    radius: CGFloat = 1
  ) {
    layer.masksToBounds = false
    layer.shadowColor = color.cgColor
    layer.shadowOpacity = opacity
    layer.shadowOffset = offSet
    layer.shadowRadius = radius
    
    layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
    layer.shouldRasterize = true
    layer.rasterizationScale = UIScreen.main.scale
  }
  
}
