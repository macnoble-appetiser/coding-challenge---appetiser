//
//  CustomImageView.swift
//  CodingChallengeByMacNoble
//
//  Created by Mac Romel D. Noble on 1/16/20.
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class CustomImageView: UIImageView {
  
  // Use this property to check if the image that was requested matches the result.
  // This is handy when reusing an image on a reusable tableViewCell
  private var urlStringKey: String?
  
  // Progress indicator while fetching an image from an external source
  private let spinner: UIActivityIndicatorView = {
    let spinner = UIActivityIndicatorView(style: .gray)
    spinner.hidesWhenStopped = true
    return spinner
  }()
  
  init() {
    super.init(frame: CGRect.zero)
    
    addSubview(spinner)
    spinner.anchorCenterSuperview()
    
    contentMode = .scaleAspectFill
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func loadImageUsingUrlString(
    urlString: String,
    completion: ((_ isOk: Bool) -> Void)? = nil) {
    
    self.urlStringKey = urlString
    
    // Set placeholder image
    self.image = R.image.placeholderIcon()
    
    if let cachedPhoto = imageCache.object(forKey: urlString as NSString) {
      image = cachedPhoto
    } else {
      getImageFromServer(urlString: urlString)
    }
  }
  
  private func getImageFromServer(urlString: String) {
    guard let url = URL(string: urlString) else { return }
    
    spinner.startAnimating()
    URLSession.shared.dataTask(with: url) { (data, response, error) in
      DispatchQueue.main.async {
        self.spinner.stopAnimating()
        
        guard let data = data else { return }
        
        if let imageToCache = UIImage(data: data) {
          imageCache.setObject(imageToCache, forKey: urlString as NSString)
          
          if self.urlStringKey == urlString {
            self.image = imageToCache
          }
        }
      }
    }.resume()
  }
}



