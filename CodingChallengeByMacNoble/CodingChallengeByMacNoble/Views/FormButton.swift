//
//  FormButton.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

@IBDesignable
class FormButton: UIButton {
  @IBInspectable var cornerRadius: CGFloat = 4 {
    didSet {
      layer.cornerRadius = cornerRadius
    }
  }

  @IBInspectable var borderColour: UIColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.3153360445) {
    didSet {
      layer.borderColor = borderColour.cgColor
    }
  }
  
  @IBInspectable var borderWidth: CGFloat = 0 {
    didSet {
      layer.borderWidth = borderWidth
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    afterInit()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }

  override func awakeFromNib() {
    super.awakeFromNib()
    afterInit()
  }

  func afterInit() {
    backgroundColor = #colorLiteral(red: 0.4196078431, green: 0.4431372549, blue: 0.4470588235, alpha: 1)
    setTitleColor(UIColor.white, for: .normal)
    titleLabel?.font = UIFont.systemFont(ofSize: 15, weight: .medium)
  }
}
