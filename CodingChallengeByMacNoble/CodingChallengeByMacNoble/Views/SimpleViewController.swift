//
//  SimpleViewController.swift
//  CodingChallengeByMacNoble
//
//  Created by Mac Romel D. Noble on 1/16/20.
//  Copyright © 2020 Appetiser Pty Ltd. All rights reserved.
//

import UIKit

class SimpleViewController: UIViewController {
    
    // Actions
    var didTapDoneButton: (() -> Void)?
    
    // UI elements
    private let bodyLabel = UILabel()
    private let mainStack = StackFactory.createVerticalStack()
    
    init(title: String?, body: String?) {
        super.init(nibName: nil, bundle: nil)
        view.backgroundColor = .white
        
        self.title = title
        bodyLabel.attributedText = body?.attributedString(withLineSpacing: 7)
        bodyLabel.numberOfLines = 0
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonHandler))

        setupConstraints()
    }
    
    @objc
    func doneButtonHandler() {
        didTapDoneButton?()
    }
    
    private func setupConstraints() {
        mainStack.addArrangedSubview(bodyLabel)
        mainStack.addArrangedSubview(UIView())
        mainStack.distribution = .equalSpacing
        view.addSubview(mainStack)
        
        let safeArea = view.safeAreaLayoutGuide
        
        mainStack.anchor(
          safeArea.topAnchor,
          right: safeArea.rightAnchor,
          bottom: safeArea.bottomAnchor,
          left: safeArea.leftAnchor,
          topConstant: 20,
          rightConstant: 15,
          bottomConstant: 20,
          leftConstant: 15,
          widthConstant: 0,
          heightConstant: 0)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
