//
//  MainCoordinator.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

// Coordinator will handle the navigation flow of the app
class MainCoordinator: Coordinator {
    
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() { }
}

extension MainCoordinator: ItemSelectable {
    func didSelectItem(_ item: Item) {
        let detailsVC = ItemDetailViewController(item: item)
        detailsVC.coordinator = self
        let nav = UINavigationController(rootViewController: detailsVC)
        navigationController.topViewController?.showDetailViewController(nav, sender: self)
    }
}

extension MainCoordinator: ItemPresentable {
    func willPresentItem(_ item: Item) {
        let simpleVC = SimpleViewController(title: item.trackName, body: item.longDescription)
        simpleVC.didTapDoneButton = { [unowned self] in
            self.navigationController.dismiss(animated: true, completion: nil)
        }
        
        let nav = UINavigationController(rootViewController: simpleVC)
        navigationController.present(nav, animated: true, completion: nil)
    }
}
