//
//  MasterDetailViewModel.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation
import RealmSwift

class MasterViewModel: DataReloadable {
    
    // Coordinator
    weak var coordinator: ItemSelectable?
    
    // Private
    private var itemList: [Item] = [] {
        didSet {
            reloadData?()
        }
    }
    private let itemRepo: ItemRepositoryInterface
    
    // Properties to conform to Data Reloadable
    var notifyLoading: ((Bool) -> Void)?
    var isLoading: Bool = true
    var reloadData: (() -> Void)?
    
    // MARK: - Init
    init(itemRepo: ItemRepositoryInterface = ItemRepository()) {
        self.itemRepo = itemRepo
    }
}

extension MasterViewModel: SearchableViewModel {
    func willSearch(with searchString: String) {
        itemRepo.getItems(searchString: searchString) { [weak self] itemList in
            self?.itemList = itemList
        }
    }
}

extension MasterViewModel: RowViewModelSource {
    var sectionCount: Int {
        get {
            return 1
        }
    }
    
    func rowCount(section: Int) -> Int {
        return itemList.count
    }
    
    func cellType() -> (AnyClass?, cellReuseId: String) {
        return (CustomCell.self, "cellId")
    }
    
    func createViewModel(indexPath: IndexPath) -> RowViewModel {
        let item = itemList[indexPath.row]
        let itemCellViewModel = ItemCellViewModel(item: item)
        return itemCellViewModel
    }
}


extension MasterViewModel: RowSelectable {
    func didSelectRow(at indexPath: IndexPath) {
        let item = itemList[indexPath.row]
        
        // Inform coordinator of action
        coordinator?.didSelectItem(item)
        
        // Save to current item to DB
        let realm = try? Realm()
        try? realm?.write {
            // Delete old item
            if let oldItem = realm?.objects(RealmCurrentItem.self) {
                realm?.delete(oldItem)
            }
            
            let newItem = RealmCurrentItem.init(value: item.map())
            realm?.add(newItem)
        }
    }
}
