//
//  User.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

enum Gender: String, Codable, Defaultable {
  
  case male, female, other
  
  static var defaultValue: Gender {
    return .other
  }
}

struct User: APIModel, Codable, Identifiable, Equatable {
  
  let id: String
  let email: String
  let username: String
  let gender: Gender?
  
}

extension User {

  /// True if this user is the one currently logged in.
  var isAppUser: Bool {
    //guard let user = App.shared.user.user else { return false }
    //return user == self
    return false
  }
  
}
