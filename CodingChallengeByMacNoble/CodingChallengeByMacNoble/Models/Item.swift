//
//  Item.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation
import RealmSwift

struct SearchResult: Codable {
  let resultCount: Int
  let results: [Item]
}

struct Item: Codable {
  let trackName: String
  let trackPrice: Double?
  let artworkUrl60: String?
  let artworkUrl100: String?
  let primaryGenreName: String?
  let releaseDate: String
  let trackTimeMillis: Int? // milliseconds
  let longDescription: String?
  
  var trackDuration: String? {
    guard let trackTimeMillis: Int = trackTimeMillis else { return nil }
    let timeInterval = TimeInterval(trackTimeMillis / 1000)
    return timeInterval.stringFromTimeInterval()
  }
  
  // A string from the combination of genre, date, & duration
  var subtitle: String {
    return [primaryGenreName, releaseDate.getFormattedDateString(), trackDuration]
      .compactMap({ $0 })
      .joined(separator: " · ")
  }
  
  func map() -> RealmItem {
    let realmItem = RealmItem()
    realmItem.trackName = trackName
    realmItem.trackPrice = RealmOptional<Double>.init(trackPrice)
    realmItem.artworkUrl60 = artworkUrl60
    realmItem.artworkUrl100 = artworkUrl100
    realmItem.primaryGenreName = primaryGenreName
    realmItem.releaseDate = releaseDate
    realmItem.trackTimeMillis = RealmOptional<Int>.init(trackTimeMillis)
    realmItem.longDescription = longDescription
    return realmItem
  }
}
