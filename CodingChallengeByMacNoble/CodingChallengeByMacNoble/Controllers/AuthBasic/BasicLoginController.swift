//
//  TraditionalLoginController.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import Material
import RxSwift
import RxCocoa

class BasicLoginController: ViewController {

  @IBOutlet var emailTextField: ErrorTextField!
  @IBOutlet var passwordTextField: ErrorTextField!
  @IBOutlet var forgotPasswordButton: FlatButton!
  @IBOutlet var registerButton: Button!
  
  @IBOutlet var loginButton: Button!
  
  private var viewModel = BasicLoginViewModel()
  private var disposeBag = DisposeBag()
  
  // TODO Move to viewModel?
  private var email: String?
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    title = "Log In"

    setupView()
    setupBindings()
  }
  
  @IBAction
  func registerButtonTapped(_ sender: AnyObject) {
    email = emailTextField.text
    //self.performSegue(withIdentifier: Segue.toTraditonalRegistrationForm, sender: nil)
  }
  
  @IBAction
  func loginButtonTapped(_ sender: AnyObject) {
    guard isValidEmail(emailTextField.text ?? "") else {
      emailTextField.error = "Enter a valid email address"
      emailTextField.isErrorRevealed = true
      return
    }
    // TODO call api
  }
  
  @IBAction
  func forgotPasswordButtonTapped(_ sender: AnyObject) {
    email = emailTextField.text
    //self.performSegue(withIdentifier: Segue.toTraditionalPasswordResetForm, sender: nil)
  }
  
}

// MARK: - TextFieldDelegate
extension BasicLoginController: TextFieldDelegate {
  
  func textFieldDidEndEditing(_ textField: UITextField) {
    (textField as? ErrorTextField)?.isErrorRevealed = false
  }
  
  func textFieldShouldClear(_ textField: UITextField) -> Bool {
    (textField as? ErrorTextField)?.isErrorRevealed = false
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    (textField as? ErrorTextField)?.isErrorRevealed = true
    return true
  }
}

private extension BasicLoginController {
  
  func setupView() {
    emailTextField.backgroundColor = nil
    emailTextField.placeholder = S.email()
    emailTextField.detail = ""
    emailTextField.isClearIconButtonEnabled = true
    emailTextField.delegate = self
    
    passwordTextField.backgroundColor = nil
    passwordTextField.placeholder = S.password()
    passwordTextField.detail = ""
    passwordTextField.clearButtonMode = .whileEditing
    passwordTextField.isVisibilityIconButtonEnabled = true
    passwordTextField.visibilityIconButton?.tintColor =
      Color.green.base.withAlphaComponent(passwordTextField.isSecureTextEntry ? 0.38 : 0.54)
    passwordTextField.delegate = self
    
    loginButton.layer.cornerRadius = loginButton.frame.size.width / 2
  }
  
  func setupBindings() {
    //emailTextField.rx.text
  }
  
}
