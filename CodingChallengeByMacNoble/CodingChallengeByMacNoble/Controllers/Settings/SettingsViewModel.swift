//
//  SettingsViewModel.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

class SettingsCollectionSection: GenericCollectionSection<SettingsCellViewModel> {}

class SettingsViewModel {
  
  private(set) var sections: [SettingsCollectionSection] = []
  
  init() {}
  
  func loadSections() {
    var section = SettingsCollectionSection()
    section.headerTitle = "About Us"
    section.items = [
      SettingsCellViewModel(.termsAndConditions, type: .disclosing, title: "Terms & Conditions"),
      SettingsCellViewModel(.privacyPolicy, type: .disclosing, title: "Privacy Policy")
    ]
    sections.append(section)
    
    section = SettingsCollectionSection()
    section.headerTitle = "Account"
    section.items = [
      SettingsCellViewModel(.logOut, type: .disclosing, title: "Log Out")
    ]
    sections.append(section)
  }
  
}

// MARK: - Helpers
extension SettingsViewModel {
  
  func cellViewModel(for indexPath: IndexPath) -> SettingsCollectionSection.ItemType {
    return sections[indexPath.section].items[indexPath.row]
  }
  
}
