//
//  FacebookViewModel.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import FBSDKLoginKit
import Foundation

typealias APIViewModelResponse = (Bool, String?) -> Void

class FacebookViewModel {
  
  // Move to view model if necessary
  // User test account: odkuxdjvhf_1574256488@tfbnw.net/Passw0rd1234
  func connectFacebook(_ viewController: UIViewController, completion: @escaping APIViewModelResponse) {
    // Facebook login manager
    let loginManager = LoginManager()
    loginManager.logIn(permissions: AppUser.facebookReadPermissions, from: viewController) { result, error in
      guard error == nil else {
        loginManager.logOut()
        return completion(false, error?.localizedDescription)
      }
      guard result?.isCancelled == false else {
        loginManager.logOut()
        return completion(false, "Facebook login cancelled")
      }
      
      // Facebook access token
      // AccessToken.current is also used to check if user previously login with facebook
      guard let accessToken = AccessToken.current else {
        loginManager.logOut()
        return completion(false, "Failed to get facebook access token")
      }
      
      print("accessToken \(accessToken)")
      // Use facebook access token to fecth user details
      // App.shared.user.connectFacebook(with: accessToken.tokenString) { (user, error) in
      //   if let apiError = error {
      //     loginManager.logOut()
      //     return completion(false, apiError.localizedDescription)
      //   }
      //   completion (true, nil)
      // }
    }
  }
}
