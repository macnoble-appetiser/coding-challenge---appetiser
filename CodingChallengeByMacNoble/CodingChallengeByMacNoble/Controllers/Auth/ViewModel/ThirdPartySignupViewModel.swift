//
//  SocialSignupViewModel.swift
//  LinkU
//
//  Created by Viktor Immanuel Calonia on 13/12/2019.
//  Copyright © 2019 Appetiser Pty Ltd. All rights reserved.
//

import Foundation
import AuthenticationServices

import RxSwift

enum ThirdPartySignups: String {
  case google
  case apple
  case facebook

  var viewModel: ThirdPartySignupViewModelProtocol? {
    switch self {
    case .apple:
      return AppleSocialSignUpViewModel()
    default:
      return nil
    }
  }
}

protocol ThirdPartySignupViewModelProtocol {
  var token: String? { get }
  func postSocial(withToken token: String) -> Single<Bool>
}

class AppleSocialSignUpViewModel: ThirdPartySignupViewModelProtocol {

  var token: String?

  func postSocial(withToken token: String) -> Single<Bool> {
    self.token = token
    return Single.create { (singleEvent) -> Disposable in
      App.shared.user.connectApple(with: token) { (user, error) in
        if let error = error {
          singleEvent(.error(error))
        } else {
          // TODO: Come up a userExistence condition here, Should be from error callback param
          let userExist = user != nil
          singleEvent(.success(userExist))
        }
      }
      return Disposables.create()
    }
  }

}
