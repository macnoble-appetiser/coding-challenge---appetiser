//
//  RegistrationController.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit
import AuthenticationServices

import FBSDKLoginKit
import PureLayout
import RxCocoa
import RxSwift
import SVProgressHUD

class RegistrationController: UIViewController {
  var email: String?

  @IBOutlet var emailTextField: FormTextField!
  @IBOutlet var passwordTextField: FormTextField!
  @IBOutlet var firstNameTextField: FormTextField!
  @IBOutlet var lastNameTextField: FormTextField!
  @IBOutlet var facebookButton: FormButton!
  @IBOutlet weak var appleLoginButtonContainer: UIView!

  fileprivate var appleLoginViewModel = AppleSocialSignUpViewModel()
  private let facebookViewModel = FacebookViewModel()
  private let disposeBag = DisposeBag()

  override func viewDidLoad() {
    super.viewDidLoad()

    buttonBindings()

    if #available(iOS 13.0, *) {
      setupAppleLogin()
    }
  }

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.setNavigationBarHidden(false, animated: animated)

    emailTextField.text = email
  }

  func buttonBindings() {
    facebookButton.rx.tap
      .subscribe(onNext: { [unowned self] in
        self.facebookViewModel.connectFacebook(
          self,
          completion: { success, errorMsg in
            guard success else {
              print(errorMsg ?? "")
              return
            }
            print("success")
        })
      })
      .disposed(by: disposeBag)
  }
}

@available(iOS 13.0, *)
extension RegistrationController: ASAuthorizationControllerDelegate,
  ASAuthorizationControllerPresentationContextProviding {

  private func setupAppleLogin() {
    let appleButton = ASAuthorizationAppleIDButton(type: .signIn, style: .whiteOutline)
    appleButton.translatesAutoresizingMaskIntoConstraints = false
    appleButton.addTarget(self, action: #selector(didTapAppleButton), for: .touchUpInside)
    appleLoginButtonContainer.addSubview(appleButton)
    appleButton.autoPinEdgesToSuperviewEdges()
  }

  @objc func didTapAppleButton() {
    let provider = ASAuthorizationAppleIDProvider()
    let request = provider.createRequest()
    request.state = UUID().uuidString
    request.requestedScopes = [.fullName, .email]

    let controller = ASAuthorizationController(authorizationRequests: [request])
    controller.presentationContextProvider = self
    controller.delegate = self
    controller.performRequests()

  }

  func authorizationController(controller: ASAuthorizationController,
                               didCompleteWithAuthorization authorization: ASAuthorization) {
    switch authorization.credential {
    case let credentials as ASAuthorizationAppleIDCredential:
      let user = credentials
      executeLoginIfNeeded(withAppleUser: user)
    default: break
    }
  }

  func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
    SVProgressHUD.showDismissableError(with: error.localizedDescription)
  }

  func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
    return view.window!
  }

  private func executeLoginIfNeeded(withAppleUser appleUser: ASAuthorizationAppleIDCredential) {
    SVProgressHUD.show()
    appleLoginViewModel.postSocial(withToken: appleUser.user)
      .subscribe(onSuccess: { [weak self] (isUserExist) in
        SVProgressHUD.dismiss()
        // MARK: Discussion: User Exist must proceed immidiately to App's Dashboard
        let nextPageSegue = isUserExist ? "<#T##String user_exist_segue_id#>": "<#T##String user_not_exist_segue_id#>"
        self?.performSegue(withIdentifier: nextPageSegue, sender: self)
      }, onError: { (error) in
        SVProgressHUD.showDismissableError(with: error.localizedDescription)
      }).disposed(by: disposeBag)
  }
}
