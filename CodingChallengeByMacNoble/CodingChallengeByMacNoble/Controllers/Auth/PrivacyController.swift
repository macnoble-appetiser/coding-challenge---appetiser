//
//  PrivacyController.swift
//  CodingChallengeByMacNoble
//
//  Created by Antonio Jr Atamosa on 01/03/2019.
//  Copyright © 2019 "Appetiser Pty Ltd". All rights reserved.
//

import UIKit

class PrivacyController: UIViewController {
  
  var policyType: String!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
      initUI()
    }
    
  func initUI() {
    if policyType == PolicyType.termsAndCondition {
      self.title = "Terms & Condition"
    } else {
      self.title = "Privacy Policy"
    }
  }
  
  @IBAction
  func backButtonTapped(_ sender: AnyObject) {
    self.navigationController?.dismiss(animated: true, completion: nil)
  }
}
