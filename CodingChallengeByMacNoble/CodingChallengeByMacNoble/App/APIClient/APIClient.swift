//
//  APIClient.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation
import Alamofire
import Valet

typealias APIClientResultClosure = (APIClientResult) -> Void
typealias APIClientUserClosure = (User?, Error?) -> Void
typealias APIClientLoginClosure = (User?, _ token: String?, Error?) -> Void

enum APIClientResult {
  case success(APIResponse)
  case failure(Error)
}

/// Our designated class that interfaces with our standard REST API Baseplate. There should only
/// be one instance of this throughout the app's lifetime.
///
/// General guidelines:
/// - Group related methods into one class extension and separate it into its own file.
///   Name it like so: `APIClient+<GROUP_NAME_HERE>.swift`.
/// - Use `-httpRequestHeaders(withAuth: FALSE)` when calling endpoints that don't require access token.
///
/// Rules for naming endpoint functions.
/// - Always start with a verb. For example: `updatePassword`, `sendInvites`, `fetchComments`.
/// - If it's more than one word, just make sure you don't end up having a sentence. It should be
///   concise and simply tells the developer reading the code in the call site what it's all about.
///
class APIClient: AppService {
  
  /// The base URL of the REST API sans the version. E.g. `https://api.domain.com/`
  private(set) var baseURL: URL
  
  /// The default version of the API to use. E.g. `v1`, `v2.1`.
  private(set) var version: String
  
  /// - parameters:
  ///   - baseURL: The base URL of the REST API sans the version. E.g. `https://api.domain.com/`
  ///   - version: URI Versioning is the most common way of versioning a REST API resource. This is
  ///     the same approach our API baseplate is adopting. While we could simply just leave it in the
  ///     baseURL, it'd be more convenient for us to segregate it right from the initialization phase.
  ///     Having it as a separate component enables our endpoints to be updated individually
  ///     as the need arises.
  ///
  public init(baseURL: URL, version: String = "v1") {
    self.baseURL = baseURL
    self.version = version
  }
  
  public func endpointURL(_ resourcePath: String, version: String? = nil) -> URL {
    return baseURL.appendingPathComponent("\(version ?? self.version)/\(resourcePath)")
  }
  
  /// Returns the default set of HTTP headers.
  ///
  /// - parameter withAuth: Whether to include Authorization header or not. Defaults to True.
  ///
  public func httpRequestHeaders(withAuth: Bool = true) -> HTTPHeaders {
    var headers = [
      "Content-Type": "application/x-www-form-urlencoded"
    ]
    if withAuth && accessToken != nil {
      headers["Authorization"] = "Bearer \(accessToken!)"
    }
    return headers
  }
  
  /// This wraps the call to `Alamofire.request(...).apiResponse(result:)`.
  ///
  /// - parameters:
  ///   - resourcePath: The path of the API resource. 
  ///   - method: The HTTP method for this API resource.
  ///   - version: Optional. Defaults to whatever the value of `self.version` property is.
  ///   - parameters: The parameters for this API resource. `nil` by default.
  ///   - encoding: The parameter encoding to use. Defaults to `URLEncoding.default`.
  ///   - headers: The HTTP headers. Defaults to calling `httpRequestHeaders(withAuth:)`.
  ///   - success: Accepts `APIResponse` instance.
  ///   - failure: Accepts `Error` instance.
  ///
  public func request(
    _ resourcePath: String,
    method: HTTPMethod,
    version: String? = nil,
    parameters: Parameters? = nil,
    encoding: ParameterEncoding = URLEncoding.default,
    headers: HTTPHeaders? = nil,
    success: @escaping (APIResponse) -> Void,
    failure: @escaping (Error) -> Void
  ) -> DataRequest {
    return Alamofire
      .request(
        endpointURL(resourcePath, version: version),
        method: method,
        parameters: parameters,
        encoding: encoding,
        headers: headers ?? httpRequestHeaders(withAuth: true)
      )
      .apiResponse(completion: { (result) in
        switch result {
        case .success(let resp):
          success(resp)
        case .failure(let error):
          failure(error)
        }
      })
  }
  
}

extension APIClient {
  
  var accessToken: String? {
    set { App.valet.setString(newValue, forKey: "keys:access_token") }
    get { return App.valet.getString(forKey: "keys:access_token") }
  }
  
}

// MARK: - APIClientError

enum APIClientError: Error {
  
  struct FailedRequestContext {
    let status: HTTPStatusCode
    let errorCode: APIResponseErrorCode
    let message: String
  }
  
  case failedRequest(FailedRequestContext)
  
  /// Indicates that a non-optional value of the given type was expected,
  /// but a null value was found.
  case dataNotFound(_ expectedType: Any.Type)
  
  case unknown
  
}

extension APIClientError: LocalizedError {
  
  var errorDescription: String? {
    switch self {
    case .failedRequest(let ctx):
      return ctx.message
    case .dataNotFound:
      return "Data expected from the server not found"
    default:
      return "An unknown error has occured"
    }
  }
  
  var failureReason: String? {
    switch self {
    case .failedRequest(let ctx):
      return "HTTPStatusCode: \(ctx.status); APIResponseErrorCode: \(ctx.errorCode); Message: \(ctx.message);"
    case .dataNotFound(let type):
      return "Expected \(String(describing: type)). Got nil instead."
    default:
      return "An unknown error has occured"
    }
  }
  
}

// MARK: - Alamofire.DataRequest

extension DataRequest {
  
  @discardableResult
  func apiResponse(
    queue: DispatchQueue? = nil,
    completion: @escaping APIClientResultClosure
  ) -> DataRequest {
    return self.responseData(queue: queue, completionHandler: { (response: DataResponse<Data>) in
      guard response.result.error == nil else {
        App.shared.recordError(response.result.error!)
        return completion(.failure(response.result.error!))
      }
      guard let responseData = response.value else {
        return completion(.failure(APIClientError.dataNotFound(Data.self)))
      }
      
      do {
        let resp = try JSONDecoder().decode(APIResponse.self, from: self.utf8Data(from: responseData))
        if let context = resp.errorContext {
          completion(.failure(APIClientError.failedRequest(context)))
        } else {
          completion(.success(resp))
        }
        
      } catch {
        App.shared.recordError(error)
        completion(.failure(error))
      }
    })
  }
  
  // TODO Throw error
  private func utf8Data(from data: Data) -> Data {
    let encoding = detectEncoding(of: data)
    guard encoding != .utf8 else { return data }
    guard let responseString = String(data: data, encoding: encoding) else {
      preconditionFailure("Could not convert data to string with encoding \(encoding.rawValue)")
    }
    guard let utf8Data = responseString.data(using: .utf8) else {
      preconditionFailure("Could not convert data to UTF-8 format")
    }
    return utf8Data
  }
  
  private func detectEncoding(of data: Data) -> String.Encoding {
    var convertedString: NSString?
    let encoding = NSString.stringEncoding(
      for: data,
      encodingOptions: nil,
      convertedString: &convertedString,
      usedLossyConversion: nil
    )
    debugLog("~~> \(encoding)")
    return String.Encoding(rawValue: encoding)
  }
  
}
