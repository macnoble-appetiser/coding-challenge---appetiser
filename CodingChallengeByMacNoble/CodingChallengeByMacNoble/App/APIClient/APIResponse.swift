//
//  APIResponse.swift
//  CodingChallengeByMacNoble
//
//  Created by "Team Appetiser" ( https://appetiser.com.au )
//  Copyright © 2018 "Appetiser Pty Ltd". All rights reserved.
//

import Foundation

/// A type representing our standard API error code.
enum APIResponseErrorCode: String, Codable, Defaultable {
  case emailAlreadyVerified
  case invalidCredentials
  case invalidEmailFormat
  case invalidInput
  case mobileAlreadyVerified
  case tokenAbsent
  case validationError
  case verificationError
  case other
  
  static var defaultValue: APIResponseErrorCode {
    return .other
  }
}

/// An object representation of the server's JSON response.
///
/// Sample success response:
///
///     {
///       "success": true,
///       "http_status": 200,
///       "message": "",
///       "data": {
///         "posts": [...]
///       }
///     }
///
/// Sample error response:
///
///     {
///       "success": false,
///       "message": "Invalid Account Key",
///       "http_status": 404,
///       "error_code": "invalidAccountKey"
///     }
///
public struct APIResponse: Decodable {
  
  /// One of the standard HTTP status codes. e.g. 200, 302, 404.
  /// Refer to this site for a full list: https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
  let status: HTTPStatusCode
  
  /// Intentionally set to Optional-Any as it could be of any type or just be nil.
  /// It's up to the call site to determine the exact type of its value.
  /// If it's a Decodable type, use the method `decodedValue(forKeyPath:decoder:)`.
  let data: Any?
  
  /// Could be a success or an error message depending on the type of result.
  let message: String?
  
  let isSuccess: Bool
  
  let errorContext: APIClientError.FailedRequestContext?
  
  enum CodingKeys: String, CodingKey {
    case status = "http_status"
    case data
    case message
    case isSuccess = "success"
    case errorCode = "error_code"
  }
  
  public init(from decoder: Decoder) throws {
    let container = try decoder.container(keyedBy: CodingKeys.self)
    
    status = try container.decode(HTTPStatusCode.self, forKey: .status)
    data = (try container.decodeIfPresent(AnyDecodable.self, forKey: .data))?.value
    isSuccess = try container.decodeIfPresent(Bool.self, forKey: .isSuccess) ?? false
    
    let errorCode = try container.decodeIfPresent(APIResponseErrorCode.self, forKey: .errorCode) ?? .other
    
    // Value of `message` could either be an array of strings or just a single string.
    let messages = try container.decodeIfPresent(AnyDecodable.self, forKey: .message)
    if let array = messages?.value as? [String] {
      message = array.first
    } else if messages?.value is String {
      message = messages!.value as? String
    } else {
      message = nil
    }
    
    if (HTTPStatusCode.badRequest...HTTPStatusCode.networkConnectTimeoutError).contains(status) {
      errorContext = APIClientError.FailedRequestContext(
        status: status,
        errorCode: errorCode,
        message: message ?? "An unknown error has occurred"
      )
    } else {
      errorContext = nil
    }
  }
  
  // MARK: Helpers
  
  /// Decodes the contents of the `data` property, if available, into its inferred Decodable type.
  ///
  /// Sample usage:
  ///
  ///     // Single object:
  ///     // {
  ///     //   "status": 200,
  ///     //   "data": {
  ///     //     "post_id": "xyz123", "title": "...", ...
  ///     //   }
  ///     // }
  ///     let post: Post! = instance.decodedValue()
  ///
  ///     // Array of objects:
  ///     // {
  ///     //   "status": 200,
  ///     //   "data": {
  ///     //     "posts": [{...}, {...}, ...]
  ///     //   }
  ///     // }
  ///     let posts: [Post]? = instance.decodedValue(forKeyPath: "posts")
  ///
  /// - parameter forKeyPath: Specify as needed. This only works with Dictionary types.
  ///       If nil, assumes `data` is for the inferred decodable type.
  /// - parameter decoder: A pre-configured JSONDecoder instance. Defaults to `GenericAPIModel`s decoder.
  ///
  /// - returns: The decoded value or nil.
  func decodedValue<T>(forKeyPath: String? = nil, decoder: JSONDecoder? = nil) -> T? where T: Decodable {
    guard var payload = data else { return nil }

    if let keyPath = forKeyPath {
      guard let d = nestedData(keyPath) else { return nil }
      payload = d
    }
    
    guard JSONSerialization.isValidJSONObject(payload) else {
      debugLog("payload is not a valid json object: \(String(describing: payload))")
      guard let val = payload as? T else { return nil }
      return val
    }
    
    do {
      let json = try JSONSerialization.data(withJSONObject: payload)
      return try (decoder ?? GenericAPIModel.decoder()).decode(T.self, from: json)
      
    } catch {
      App.shared.recordError(error, info: ["keyPath": forKeyPath ?? "n/a"])
      return nil
    }
  }
  
  /// Returns the data at the given `keyPath`. Nil if path doesn't exist.
  private func nestedData(_ keyPath: String) -> Any? {
    guard let payload = data, !keyPath.isEmpty else { return nil }
    guard let dict = payload as? [String: Any] else { return nil }
    return dict[keyPath: keyPath] as Any
  }
  
}
