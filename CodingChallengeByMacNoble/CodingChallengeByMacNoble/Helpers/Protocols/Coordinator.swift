//
//  Coordinator.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 10/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get }
    func start()
}

protocol ItemSelectable: class {
    func didSelectItem(_ item: Item)
}

protocol ItemPresentable: class {
    func willPresentItem(_ item: Item)
}
