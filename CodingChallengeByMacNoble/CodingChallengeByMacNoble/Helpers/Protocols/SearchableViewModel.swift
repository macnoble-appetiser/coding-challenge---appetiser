//
//  SearchableViewModel.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import Foundation

protocol SearchableViewModel {
    func willSearch(with searchString: String)
}
