//
//  SearchableProtocol.swift
//  AppetizerCodingChallenge
//
//  Created by Mac Romel D. Noble on 09/10/2019.
//  Copyright © 2019 Mac Romel D. Noble. All rights reserved.
//

import UIKit

protocol Searchable {
    var searchController: UISearchController { get set }
}

extension Searchable where Self: UIViewController {
    func configureSearch() {
        searchController.obscuresBackgroundDuringPresentation = false
        navigationItem.hidesSearchBarWhenScrolling = false
        
        definesPresentationContext = true
        
        navigationItem.searchController = searchController
        searchController.hidesNavigationBarDuringPresentation = true
    }
}
